import { TestBed } from '@angular/core/testing';

import { GetTopServiceService } from './get-top-service.service';

describe('GetTopServiceService', () => {
  let service: GetTopServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetTopServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
