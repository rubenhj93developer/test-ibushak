import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GetTopServiceService {

  public apiUrl:string;

  constructor(private httpClient:HttpClient) { 
    this.apiUrl = 'http://localhost:3000/top';
  }

  /**
   * Metodo para obtener datos de api top 1000 celulares
   * 
   * @return {Observable} 
   * @author Ruben Hernandez Jimenez
   */

  public getTop(){

    return this.httpClient.get(this.apiUrl)
      .pipe(
        map((res:any) => {
          return res.data;
        })
    );
    
  }


}
