import { Component, OnInit } from '@angular/core';
import { GetTopServiceService } from './services/get-top-service.service';
import { Top } from './interfaces/top.interfaces';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public title:string;
  public data:Array<Top>;
  public loading:boolean;

  constructor(private getTopService:GetTopServiceService){

    this.title    = 'Top 1000 de celulares con menor precio';
    this.data     = [];

  }

  ngOnInit(){

    /*
    |------------------------------------
    | NOS SUBSCRIBIMOS PARA OBTENER DATA
    |------------------------------------
    */

    this.getTopService.getTop().subscribe(
      (data:Array<Top>) =>{
        this.data = data;
      },(errors:any)=>{
        console.log(errors);
      }
    );

  }
  
}
