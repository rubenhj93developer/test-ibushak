export interface Top{
    seller_id: number,
    seller_name: string,
    marca: string,
    envio_gratis: boolean,
    tipo_de_logistica: string,
    lugar_de_operacion_del_seller: string,
    condicion_del_articulo: string,
    rango_de_precios: number
}