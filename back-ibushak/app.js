/*
|---------------------------------------------
|   importamos librerias
|---------------------------------------------
*/

var express  = require('express');


/*
|---------------------------------------------
|  inicializamos express
|---------------------------------------------
*/

var app = express();


/*
|---------------------------------------------
|  middleware cors
|---------------------------------------------
*/

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods","POST, GET, PUT, DELETE, OPTIONS");
    next();
});

/*
|---------------------------------------------
|  importamos rutas
|---------------------------------------------
*/

var topRoutes  = require('./routes/top');
app.use('/top',topRoutes);


/*
|---------------------------------------------
|  especificamos puerto de salida
|---------------------------------------------
*/

app.listen(3000, ()=>{// usamos funcion de flecha
    console.log('express corriendo en el puerto 3000: \x1b[32m%s\x1b[0m','online');
});
