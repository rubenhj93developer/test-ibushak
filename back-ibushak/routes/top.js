/*
|---------------------------------------------
|   importamos librerias
|---------------------------------------------
*/


const express           = require('express');
var app                 = express();
var configMercadoLibre  = require('../config/config-mercado-libre').config;
const axios             = require('axios');


/**
 * METODO PARA OBTENER VALOR DEL ATTRIBUTO
 *
 * @param {Array} arrAttributes
 * @param {String} name
 * @return {String} attribute
 * @author Ruben Hernandez Jimenez
 */

function getAttributes(arrAttributes,name){

    var attribute = '';

    if(arrAttributes){
        for (var i = 0; i < arrAttributes.length; i++) {

            var attr    = arrAttributes[i];
            var value   = attr.values || null;

            if(attr.name == name && value != null){
                attribute = value[0].name;
                break;
            }

        }
    }

    return attribute;

}

/**
 * METODO PARA CONSULTAR API MERCADO LIBRE
 *
 * @param {String} pathApi
 * @param {Number} page
 * @return {Promise}
 * @author Ruben Hernandez Jimenez
 */

function getData(pathApi,page){
    return axios.get(pathApi+'?category=MLM1055&search_type=scan&sort=price_asc&offset='+page);
}

/**
 * METODO PARA OBTENER INFORMACION NECESARIA
 *
 * @param {Array} fullData
 * @return {Array} data
 * @author Ruben Hernandez Jimenez
 */

function orderData(fullData){

    var data = [];
    
    for (var i = 0; i < fullData.length; i++) {

        page = fullData[i].data.results;

        for (var j = 0; j < page.length; j++) {

            var item = page[j];

            data.push({
                'seller_id':item.seller.id,
                'seller_name':item.title,
                'marca':getAttributes(item.attributes,'Marca'),
                'envio_gratis':item.shipping.free_shipping,
                'tipo_de_logistica':item.shipping.logistic_type,
                'lugar_de_operacion_del_seller':item.seller_address.country.name+' - '+item.seller_address.state.name,
                'condicion_del_articulo':getAttributes(item.attributes,'Condición del ítem'),
                'rango_de_precios':item.price,
            });

        }
        
    }

    data.sort((a,b) => (a.rango_de_precios > b.rango_de_precios) ? 1 : ((b.rango_de_precios > a.rango_de_precios) ? -1 : 0)); 

    return data;

}

/*
|---------------------------------------------
|   rutas
|---------------------------------------------
*/


app.get('/',(req,res)=>{

    var queryParams = '/sites/MLM/search/';
    var pathApi     = configMercadoLibre.url+queryParams;
    var data        = [];
    let promises    = [];

    for (let i = 0; i < 20; i++) {
        promises.push(getData(pathApi,i));// buscamos 20 paginas con 50 registros cada una
    }

    axios.all(promises).then(axios.spread((...fullData) => {

        data = orderData(fullData);
        
        res.status(200).json({ //response
            'status':'oks',
            'message': 'Datos listos',
            'elements':data.length,
            'data':data
        });

    }));

});

module.exports = app;
